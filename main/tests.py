from django.test import TestCase
from .models import Article, ZomboScore
from django.db.models.functions import Lower
from django.db.models import Func, F, Count, Value, FloatField, TextField
from django.db.models.expressions import RawSQL


class ZombodbTests(TestCase):
    def setUp(self):
        self.a1 = Article(text='Performs an SQL delete query on all rows in the QuerySet '
                               'and returns the number of objects deleted and a dictionary '
                               'with the number of deletions per object type.')
        self.a1.save()
        self.a2 = Article(text='The delete() is applied instantly. You cannot call delete() on a QuerySet that'
                          ' has had a slice taken or can otherwise no longer be filtered.')
        self.a2.save()

    def tearDown(self):
        self.a1.delete()
        self.a2.delete()

    def test_select(self):
        articles_all = Article.objects.all()
        self.assertSequenceEqual((self.a1, self.a2), articles_all)

    def test_search(self):
        articles_searched = Article.objects.filter(text__zombo_search='(call OR box)')
        print(articles_searched.query)
        self.assertEqual(self.a2, articles_searched[0])
        self.assertEqual(self.a2.text, articles_searched[0].text)

    def test_values(self):
        articles_searched = Article.objects.filter(text__zombo_search='(call OR box)').values('text')
        self.assertEqual(self.a2.text, articles_searched[0]['text'])

    def test_highlight(self):
        articles_searched = (Article.objects
                             .filter(text__zombo_search='delete')
                             .values(highlight_text=RawSQL("zdb.highlight(ctid, %s)", ('text',))))
        print(articles_searched.query)
        self.assertTrue(articles_searched[0]['highlight_text'])
        self.assertIn('<em>', articles_searched[0]['highlight_text'][0])

        # print(articles_searched.query)
        # print(articles_searched)

    def test_score(self):
        articles_searched = (Article.objects
                             .filter(text__zombo_search='delete')
                             .annotate(score=ZomboScore())
                             .values_list(F('score'))
                             .order_by('-score'))
        print(articles_searched.query)
        self.assertEqual(len(articles_searched), 2)
